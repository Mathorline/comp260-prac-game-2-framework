﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {
	/*Questions to ask Dan
	Is a delegate like overloading a method?
	
	*/
	public delegate void ScoreGoalHandler(int player);
	public ScoreGoalHandler scoreGoalEvent;
	public int player;

	public AudioClip scoreClip;
	private AudioSource audio;
	// week 8 stuff



	void Start() {
		audio = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider collider) { // page 2
		// play score sound
		audio.PlayOneShot(scoreClip);

		// reset the puck to its starting position
		PuckControl puck =         
			collider.gameObject.GetComponent<PuckControl>();
		puck.ResetPosition();

		// notify event handlers if there are any
		if (scoreGoalEvent != null) {
			scoreGoalEvent(player);
		}
	}

	void Update () {
	}
}
