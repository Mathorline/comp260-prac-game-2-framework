﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Scorekeeper : MonoBehaviour {
	public int scorePerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;
	public Text win;
	// Use this for initialization

	void Start() {
		//haha
		Goal[] goals = FindObjectsOfType<Goal> ();
		for(int i=0;i < goals.Length;i++){
			goals[i].scoreGoalEvent += OnScoreGoal;
			//score [i] = 0;
		}
		// reset the scores to 0
		for(int i=0;i< score.Length;i++){
			score [i] = 0;
			scoreText [i].text = "0";
		}
	}


	public void OnScoreGoal(int player){
		score [player] += scorePerGoal;
		scoreText [player].text = score [player].ToString ();
		Debug.Log ("Player " + player + ": " + score [player]);
		if (score [player] >= 10) {
			Time.timeScale = 0.0f;
			win.text = "Player " + player + " wins!";
		}
	}
}
