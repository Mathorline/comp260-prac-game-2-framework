﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;

	}
	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XY plane
		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}
	void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}
	/*void FixedUpdate () { // Moving the Paddle: Approach #1- rigidbody.MovePosition 
		Vector3 pos = GetMousePosition();
		rigidbody.MovePosition(pos);
	}*/


	//Moving the Paddle: Approach #2 - rigidbody.velocity, w/ avoid overshoot (imporovement on original code on pg 14 of prac document).
	public float speed = 20f; // needed in both version of Approach #2.
	void FixedUpdate () {
		/*
		Vector3 pos = GetMousePosition();
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}
		*/
		rigidbody.velocity = direction*maxSpeed;
	}
	// end block



	/*
	//Moving the Paddle: Approach #3 - rigidbody.AddForce
	public float force = 10f;

	void FixedUpdate () {
		Vector3 pos = GetMousePosition();
		Vector3 dir = pos - rigidbody.position;

		rigidbody.AddForce(dir.normalized * force);
	}
	// end block
	*/
	
	// Update is called once per frame
	private Vector3 direction;
	public float maxSpeed = 5.0f;
	void Update () {
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");
	}
}
// For me, their was no difference between Continuous(Static and Dynamic) and Discrete, however incase i'm not able to break the game, I will leave the collission detection to Continous (static) in case someone else can.
// I also did the same for the puck.
/*
 * Demonstrate your work questions.
 * 1) Transform simply allows you to manipulate the position, rotation and scale of an object. This means that you need to do more coding and monitoring the change in variables (hence why debug is used so often for transform). Transform is somewhat limited in that you need to code in paramaters such as gravity in Newtons etc to make it realistic. Rigidbody acts upon the physical object, taking into consideration physics. This is why we turned off the "Use Gravity" option. I personally prefer Rigidbody over transform but can see the value in both options.
 * 2) MovePosition is similar to transform in that it moves the obejects postion to the given argument but is different in that it updates all physical effects of the movement (such as if a collission has occured). Velocity moves the paddle to the mouse with respect to velocity it creates another vector to imitate movement (like in a Multimedia information system).  .AddForce acted like an object orbiting around the mouse. The eccentricity of the orbit was quite low approx <=0.125.  
 * 3) Look at unity scene. 
*/
