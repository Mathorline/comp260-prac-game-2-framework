﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


// got up to triggers

[RequireComponent(typeof(AudioSource))]
// got up to Layers on the prac sheet.
public class PuckControl : MonoBehaviour {
	public Transform startingPos;
	private Rigidbody rigidbody;
	public AudioClip wallCollideClip;
	private AudioSource audio;

	// added code here

	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody>();
		ResetPosition();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	//changed stuff here.
	void OnCollisionEnter(Collision collision) {
		// check what we have hit
		if (paddleLayer.Contains(collision.gameObject)) {
			// hit the paddle
			audio.PlayOneShot(paddleCollideClip);
		}
		else {
			// hit something else
			audio.PlayOneShot(wallCollideClip);
		}
	}
	// end
	void OnCollisionStay(Collision collision) {
		Debug.Log("Collision Stay: " + collision.gameObject.name);
	}

	void OnCollisionExit(Collision collision) {
		Debug.Log("Collision Exit: " + collision.gameObject.name);
	}

	// Puck reset.
	public void ResetPosition() {
		// teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
		//stop it from moving
		rigidbody.velocity= Vector3.zero;
	}


}
// 1) Collision events work when objects colide with eachother.