﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AI : MonoBehaviour {
	private Rigidbody rigidbody;
	public Transform target;
	private Transform puck;
	public Transform startingPos;
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
		PuckControl p = FindObjectOfType<PuckControl> ();
		puck = p.transform;

	}
		

	public float speed = 20f; 
	void FixedUpdate () {



		Vector2 direction = target.position - transform.position;
		Vector2 vel = direction * speed;

		rigidbody.velocity = vel;
	}
	/*void OnCollisionEnter(Collision collision) {
		// check what we have hit
		if (AI.Contains(collision.gameObject)) {
			// hit the paddle
			//audio.PlayOneShot(paddleCollideClip);
		}
		else {
			// hit something else
			//audio.PlayOneShot(wallCollideClip);
		}
	}
	*/
	public void ResetPosition() {
		// teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
		//stop it from moving
		rigidbody.velocity= Vector3.zero;
	}	
	void Update () {

	}
}